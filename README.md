# simpleproxy

Very simple dockerized TCP proxy

This container can be used to proxy TCP connections between services in different networks as needed.

## Parameters:

**LISTEN_PORT**: Port in which the daemon will listen to connections. The container should redirect incoming connections to this port.

**REMOTE_HOST**: Address of the server that the connections will be proxied to.

**REMOTE_PORT**: Port of the server that the connections will be proxied to.
