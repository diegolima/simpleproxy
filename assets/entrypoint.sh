#!/bin/bash -e

PROXY_CMD="$(which simpleproxy) -v"

[[ -n "${LISTEN_PORT}" ]] && PROXY_CMD="${PROXY_CMD} -L ${LISTEN_PORT}" || ( echo "Missing a LISTEN_PORT environment variable" && exit 1 )
[[ -n "${REMOTE_HOST}" && -n "${REMOTE_PORT}" ]] && PROXY_CMD="${PROXY_CMD} -R ${REMOTE_HOST}:${REMOTE_PORT}" || ( echo "Missing a REMOTE_HOST or REMOTE_PORT environment variable" && exit 1 )

${PROXY_CMD}
