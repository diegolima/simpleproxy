FROM ubuntu:bionic

RUN apt-get update \
  && apt-get install -y \ 
    simpleproxy \
  && rm -Rf /var/lib/apt/lists/*

COPY assets/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD ["/entrypoint.sh"]
